import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GestClientComponent } from './gest-client.component';

describe('GestClientComponent', () => {
  let component: GestClientComponent;
  let fixture: ComponentFixture<GestClientComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GestClientComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(GestClientComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
