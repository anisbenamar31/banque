import { Component, OnInit } from '@angular/core';
import { ClientService } from '../services/client.service';
import { SweetalertService } from '../services/sweetalert.service';

@Component({
  selector: 'app-gest-client',
  templateUrl: './gest-client.component.html',
  styleUrls: ['./gest-client.component.css']
})
export class GestClientComponent implements OnInit {
  clientsExpanded: boolean = true;
  clientObj = {
    cin: null,
    nom: null,
    prenom: null,
  };
  isModifClient = false;
  listClients=[]

  search

  constructor(public clientService :ClientService, public sweetAlertService :SweetalertService) { }

  ngOnInit(): void {
    
    this.init()
  }

  init(){
    
    this.clientObj = {
      cin: null,
      nom: null,
      prenom: null,
    };

    this.isModifClient = false;
    this.getAllClient()
  }

  toggleExpansion(block) {
    if (block === 'clients') {
      this.clientsExpanded = !this.clientsExpanded;
    } 
  }


  getAllClient(){
    this.clientService.getAll().subscribe({
      next: (res: any) => {
        console.log(res);
        
        this.listClients = res.reverse();
      },
      error: (err) => {
        console.log(err);
      },
    });
  }
  addClient(){
    if (this.clientObj.cin)
    this.clientService.post(this.clientObj).subscribe({
      next: (res) => {
        this.getAllClient();
        this.init();
        this.sweetAlertService.quickALert('success','Client  ajouter avec Succés')
      },
      error: (err) => {
        this.init();
        this.sweetAlertService.quickALert('danger','Client Non Enregister')
      },
    });
  else 
  this.sweetAlertService.quickALert('warning','veuillez entrer le Cin')
  }

  updateClient(cin) {
    if (this.clientObj.nom && this.clientObj.prenom ) {
    this.sweetAlertService.confirmAlert({
      title: "Update Confirmation",
      text: "Are you sure you want to update this item?",
      confirmButtonText: "Yes, update it!",
      afterConfirmTitle:"Updated!",
      afterConfirmText:"Your item has been updated."
    }, () => {
      console.log(this.clientObj);
      
      delete this.clientObj['id']
      delete this.clientObj['cin']
      this.clientService.put(cin, this.clientObj).subscribe(res => {
        // Update operation successful
        console.log('Item updated!');
        
        this.getAllClient();
        this.init();
      },
      error => {
        this.getAllClient();
        this.init();
        console.error('Error updating item:', error);
      });
    });
  }
  else
  this.sweetAlertService.quickALert('warning','please insert nom and prenom')
  }
  

  clickToUpdateClient(client){
    this.isModifClient = true;
    this.clientObj = client;
  }

  deleteClient(cin) {
    this.sweetAlertService.confirmAlert({
      title: "Delete Confirmation",
      text: "Are you sure you want to delete this item?",
      confirmButtonText: "Yes, delete it!"
    }, () => {
      // Your delete logic here
      this.clientService.delete(cin).subscribe(res => {
        console.log('Item deleted!');
        
        this.getAllClient();
      },
      error => {
        this.getAllClient();
        console.error('Error deleting item:', error);
      });
    });
  }
  

}
