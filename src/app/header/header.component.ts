import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  routeUrl='/client'
  constructor(public router :Router) { }

  ngOnInit(): void {
    
  }

  navigateTo(route){
    this.routeUrl= route
    this.router.navigateByUrl(route)
  }

}
