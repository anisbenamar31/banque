import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { GestClientComponent } from './gest-client/gest-client.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule} from '@angular/common/http';
import { SweetalertComponent } from './features/sweetalert/sweetalert.component';
import { GestCompteComponent } from './gest-compte/gest-compte.component';
import { FilterPerAnykeyPipe } from './pipes/filter-per-anykey.pipe';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    GestClientComponent,
    SweetalertComponent,
    GestCompteComponent,
    FilterPerAnykeyPipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    BrowserAnimationsModule,
    HttpClientModule

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
