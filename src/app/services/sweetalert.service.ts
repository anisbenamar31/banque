import { Injectable } from '@angular/core';
import Swal from 'sweetalert2';

@Injectable({
  providedIn: 'root'
})
export class SweetalertService {

  constructor() { }


  quickALert(type,message){
    const Toast = Swal.mixin({
      toast: true,
      position: "top-end",
      showConfirmButton: false,
      timer: 3000,
      timerProgressBar: true,
      didOpen: (toast) => {
        toast.onmouseenter = Swal.stopTimer;
        toast.onmouseleave = Swal.resumeTimer;
      }
    });
    Toast.fire({
      icon: type,
      title: message
    });
  }
  confirmAlert(options: { title?: string, text?: string, confirmButtonText?: string , afterConfirmTitle?:string ,afterConfirmText?:string}, onConfirm: () => void) {
    Swal.fire({
      title: options.title || "Are you sure?",
      text: options.text || "You won't be able to revert this!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: options.confirmButtonText || "Yes, delete it!"
    }).then((result) => {
      if (result.isConfirmed) {
        Swal.fire({
          title: options.afterConfirmTitle|| "Deleted!",
          text: options.afterConfirmText ||  "Your item has been deleted.",
          icon: "success"
        });
        // Call the provided callback function
        onConfirm();
      }
    });
  }
}
