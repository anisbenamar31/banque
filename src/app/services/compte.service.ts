import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CompteService {

  domaine = environment.apiEndPoint;
  flatEndPoint = environment.flatEndPoint;
  api = this.domaine + '/compte/';

  constructor(
    private http: HttpClient
  
  ) {

  }

    // compte
    getAll() {
      return this.http.get(this.api+'all');
    }
    getByRib(rib) {
      return this.http.get(this.api   + rib);
    }
    post(data) {
      return this.http.post(this.api , data);
    }
    put(id, data) {
      return this.http.put(this.api  + id, data);
    }
    delete(id) {
      return this.http.delete(this.api  + id);
    }
}
