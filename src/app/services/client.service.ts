import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ClientService {
  domaine = environment.apiEndPoint;
  flatEndPoint = environment.flatEndPoint;
  api = this.domaine + '/client/';

  constructor(
    private http: HttpClient
  
  ) {

  }

    // client
    getAll() {
      return this.http.get(this.api+'all');
    }
    getByCin(cin) {
      return this.http.get(this.api   + cin);
    }
    post(data) {
      return this.http.post(this.api , data);
    }
    put(id, data) {
      return this.http.put(this.api  + id, data);
    }
    delete(id) {
      return this.http.delete(this.api  + id);
    }

}
