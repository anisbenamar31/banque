import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { GestClientComponent } from './gest-client/gest-client.component';
import { GestCompteComponent } from './gest-compte/gest-compte.component';

const routes: Routes = [
  {
    path: 'client',
    component: GestClientComponent,
  },

  {
    path: 'compte',
    component: GestCompteComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
