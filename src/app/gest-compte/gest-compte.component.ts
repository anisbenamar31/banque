import { Component, OnInit } from '@angular/core';
import { CompteService } from '../services/compte.service';
import { SweetalertService } from '../services/sweetalert.service';
import { ClientService } from '../services/client.service';

@Component({
  selector: 'app-gest-compte',
  templateUrl: './gest-compte.component.html',
  styleUrls: ['./gest-compte.component.css']
})
export class GestCompteComponent implements OnInit {

  comptesExpanded: boolean = true;
  compteObj = {
    solde: 20,
    client: {
        cin: null
    }
}
  isModifcompte = false;
  listcomptes=[]
  listClients=[]

  search
  

  constructor(public compteService :CompteService,public clientService :ClientService, public sweetAlertService :SweetalertService,) { }

  ngOnInit(): void {
    this.getALLClientsToSelect()
    this.init()
  }

  init(){
    this.getAllcompte()

    this.compteObj = {
      solde: 20,
      client: {
          cin: null
      }
    };

    this.isModifcompte = false;
  }

  toggleExpansion(block) {
    if (block === 'comptes') {
      this.comptesExpanded = !this.comptesExpanded;
    } 
  }


  getALLClientsToSelect(){
    this.clientService.getAll().subscribe((res:any)=>{
      this.listClients =res
    })
  }

  getAllcompte(){
    this.compteService.getAll().subscribe({
      next: (res: any) => {
        console.log(res);
        
        this.listcomptes = res.reverse();
      },
      error: (err) => {
        console.log(err);
      },
    });
  }
  addcompte(){
    if (this.compteObj.client.cin)
    this.compteService.post(this.compteObj).subscribe({
      next: (res) => {
        this.getAllcompte();
        this.init();
        this.sweetAlertService.quickALert('success','compte  ajouter avec Succés')
      },
      error: (err) => {
        this.init();
        this.sweetAlertService.quickALert('danger','compte Non Enregister')
      },
    });
  else 
  this.sweetAlertService.quickALert('warning','veuillez entrer le Cin')
  }

  updatecompte(rib) {
    if (this.compteObj.solde && this.compteObj.solde>0 ) {
    this.sweetAlertService.confirmAlert({
      title: "Update Confirmation",
      text: "Are you sure you want to update this item?",
      confirmButtonText: "Yes, update it!",
      afterConfirmTitle:"Updated!",
      afterConfirmText:"Your item has been updated."
    }, () => {
      console.log(this.compteObj);
      
      delete this.compteObj['client']
      this.compteService.put(rib, this.compteObj).subscribe(res => {
        // Update operation successful
        console.log('Item updated!');
        
        this.getAllcompte();
        this.init();
      },
      error => {
        this.getAllcompte();
        this.init();
        console.error('Error updating item:', error);
      });
    });
  }
  else
  this.sweetAlertService.quickALert('warning','please enter valide solde')
  }

  clickToUpdatecompte(compte){
    this.isModifcompte = true
    this.compteObj = compte;
  }


  deletecompte(rib) {
    this.sweetAlertService.confirmAlert({
      title: "Delete Confirmation",
      text: "Are you sure you want to delete this item?",
      confirmButtonText: "Yes, delete it!"
    }, () => {
      // Your delete logic here
      this.compteService.delete(rib).subscribe(res => {
        console.log('Item deleted!');
        
        this.getAllcompte()
      },
      error => {
        this.getAllcompte();
        console.error('Error deleting item:', error);
      });
    });
  }
  

}
