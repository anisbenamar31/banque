import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GestCompteComponent } from './gest-compte.component';

describe('GestCompteComponent', () => {
  let component: GestCompteComponent;
  let fixture: ComponentFixture<GestCompteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GestCompteComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(GestCompteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
