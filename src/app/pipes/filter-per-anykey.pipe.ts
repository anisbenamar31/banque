import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterPerAnykey',
})
export class FilterPerAnykeyPipe implements PipeTransform {
  transform(items: any[], searchText: string, keysToFilter?: string[]): any[] {
    if (!items) return [];
    if (!searchText) return items;

    return items.filter((item) => {
      const keys = keysToFilter || Object.keys(item);
      return keys.some((key) => {
        return String(item[key])
          .toLowerCase()
          .includes(searchText.toLowerCase());
      });
    });
  }
}